@extends('plantilla')
@section('contenido')
@if($mensaje = Session::get('success'))
    <div class="row" id="divok">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-success">
                <i class="fa-solid fa-check"></i>  {{$mensaje}}
            </div>
        </div>
    </div>
@endif
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4">
            <div class="d-grid mx-auto">
                <button class="btn btn-dark"  data-bs-toggle="modal" data-bs-target="#modalPeliculas">
                    <i class="fa-solid fa-circle-plus"></i> Añadir Categoria
                </button>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-lg-8 offset-lg-2"> 
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Categoria</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody class="table-group-divider">
                        @php $i=1; @endphp
                        @foreach($peliculas as $row)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $row->pelicula }}</td>
                            <td>
                                <a href="{{ url('peliculas',[$row]) }}" class="btn btn-warning"><i class="fa-solid fa-edit"></i></a>
                            </td>
                            <td>
                                <form method="POST" id="frm_{{$row->id}}" action="{{ route('peliculas.destroy',$row->id)}}">
                                    @csrf
                                    @method('DELETE')   
                                    <button onclick="datos('{{ $row->id }}','{{ $row->pelicula }}')" type="button" class="btn btn-danger"><i class="fa-solid fa-trash " data-bs-toggle="modal" data-bs-target="#modalEliminar"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPeliculas" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Añadir Categoria</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="frmPeliculas" method="POST" action="{{ url("peliculas")}}">
                    @csrf
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                        <input type="text" name="pelicula" class="form-control" maxlength="50" placeholder="Categoria" required>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
              <button type="button" id="btnCerrar" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" tabindex="-1" id="modalEliminar">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="fa-solid fa-warning"> </i> Eliminar</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <p>¿Segúro de eliminar la categoría <label id="lbl_nombre"> </label> ?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fa-solid fa-ban"></i> Cancelar</button>
              <button id="btnEliminar" type="button" class="btn btn-warning"><i class="fa-solid fa-check"></i> Si, Eliminar categoría</button>
            </div>
          </div>
        </div>
      </div>
    
@endsection 
@section('js')
@vite('resources/js/alertas.js')
@endsection