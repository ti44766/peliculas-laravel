@extends('plantilla')
@section('contenido')
<div class="row mt-3">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header bg-dark text-white">Editar Pelicula</div>
            <div class="card-body">
                <form id="frmPeliculas" method="POST" action="{{ url("categorias",[$categoria])}}">
                    @csrf
                    @method('PUT')
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-film"></i></span>
                        <input type="text" name="nombre" value="{{$categoria->nombre}}" class="form-control" maxlength="50" placeholder="Nombre" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-comments"></i></span>
                        <input type="text" name="sinopsis" value="{{$categoria->sinopsis}}" class="form-control" maxlength="150" placeholder="Sinopsis" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-user-tie"></i></span>
                        <input type="text" name="director" value="{{$categoria->director}}" class="form-control" maxlength="50" placeholder="Director" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-hashtag"></i></span>
                        <input type="number" name="calificacion" value="{{$categoria->calificacion}}" class="form-control" maxlength="50" placeholder="Calificacion" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-calendar-days"></i></span>
                        <input type="date" name="estreno" value="{{$categoria->estreno}}" class="form-control" maxlength="50" placeholder="Estreno" required>
                    </div>
                    <div class="input-group mb-3">
                        <span class="input-group-text"><i class="fa-solid fa-globe"></i></span>
                       <select name="id_peliculas" class="form-select" required>
                            <option value="">Categorias XD</option>
                            @foreach($peliculas as $row)
                                @if ($row->id == $categoria->id_peliculas)
                                    <option selected value="{{$row->id}}">{{ $row->pelicula}}</option>
                                @else
                                <option value="{{$row->id}}">{{ $row->pelicula}}</option>
                                
                                @endif
                            @endforeach
                       </select>
                    </div>
                    <div class="d-grid col-6 mx-auto">
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection