<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorias;
use App\Models\Peliculas;
use Illuminate\Validation\Rule;
class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categorias = Categorias::select('categorias.id','nombre','sinopsis','director','calificacion','estreno','pelicula')
        ->join('peliculas','peliculas.id','=','categorias.id_peliculas')->get();
        $peliculas = Peliculas::all();
        return view('categorias',compact('categorias','peliculas'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $categoria = new Categorias($request->input());
        $categoria->saveOrFail();
        return redirect()->route('categorias.index')->with('success','Pelicula Creada');
        /**$request->validate([
            'calificacion' => 'required|numeric|between:1,10',
        ]);
        $categoria = new Categorias($request->input());
        $categoria->saveOrFail();
        return redirect()->route('categorias.index')->with('success', 'Categoría creada');*/
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $categoria = Categorias::find($id);
        $peliculas = Peliculas::all();
        return view('editCategorias',compact('categoria','peliculas'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $categoria = Categorias::find($id);
        $categoria->fill($request->input())->saveOrFail();
        return redirect()->route('categorias.index')->with('success','Pelicula actualizada');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $categoria = Categorias::find($id);
        $categoria->delete();
        return redirect()->route('categorias.index')->with('success','Pelicula Eliminada');
    }
}
