<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peliculas;

class PeliculasController extends Controller
{
    public function index()
    {
        $peliculas = Peliculas::all();
        return view('peliculas',compact('peliculas'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'pelicula' => 'required|unique:peliculas,pelicula|max:50',
        ]);
    
        $pelicula = new Peliculas($request->input());
        $pelicula->saveOrFail();    
        return redirect()->route('peliculas.index')->with('success', 'Categoría Creada');
    }
    

    public function show(string $id)
    {
        $pelicula = Peliculas::find($id);
        return view('editPelicula',compact('pelicula'));
    }

    public function edit(string $id)
    {
        //
    }

    public function update(Request $request, string $id)
    {
        $pelicula = Peliculas::find($id);
        $pelicula->fill($request->input())->saveOrFail();
        return redirect()->route('peliculas.index')->with('success','Categoría Actualizada');
    }

    public function destroy(string $id)
    {
        $pelicula = Peliculas::find($id);
        $pelicula->delete();
        return redirect()->route('peliculas.index')->with('success','Categoría eliminada');
    }
}
